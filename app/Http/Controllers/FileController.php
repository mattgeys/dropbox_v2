<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use DB;
use Illuminate\Support\Facades\Storage;
use Response;

class FileController extends Controller
{
    public function storeFile(Request $request)
    {
        session_start();
        $this->validate($request, [
            'file' => 'required',
        ]);

        $fileName = empty($request->input('name')) ? $request->file->getClientOriginalName() : $request->input('name');
        $request->file->storeAs('public/upload', $fileName);

        $file = new File;
        $file->name = $fileName;
        $file->user_id = DB::table('users')->where(['email' => $_SESSION['email']])->value('id');
        $file->save();
        session_abort();

        return redirect('/bestanden')->with('success', 'Bestand geüpload!');

    }

    public function getFiles()
    {
        session_start();
        if(isset($_SESSION['email'])) {
            $id = DB::table('users')->where(['email' => $_SESSION['email']])->value('id');
            $files = DB::table('files')->where(['user_id' => $id])->get();

            session_abort();
            return view('myfiles')->with('files', $files);
        }
        else {
            return redirect('/inloggen')->with('fail', 'Je bent niet ingelogd');
        }
    }

    public function download($filename)
    {
        try {
            return Response::download(Storage_path('app/public/upload/' . $filename), null, [], null);

        } catch (\Exception $e) {
            return 'File not found';
        }
        //return $filename;
    }

    public function delete($fileId)
    {
        $filename = DB::table('files')->where(['id' => $fileId])->value('name');
        if (Storage::delete('public/upload/' . $filename)) {
            File::find($fileId)->delete();
            return redirect('/bestanden')->with('success', 'Bestand verwijderd!');
        }

    }
}
