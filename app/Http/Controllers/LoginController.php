<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    public function login (Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $checkLogin = DB::table('users')->where(['email' => $email, 'password' => $password])->get();

        if(count($checkLogin) > 0) {
            session_start();
            $_SESSION['email'] = $email;
            return redirect('/');
        }
        else {
            return redirect('/inloggen')->with('fail', 'Email of wachtwoord onjuist!');
        }
    }

    public function logout() {
        session_start();
        session_destroy();

        return redirect('/');
    }
}
