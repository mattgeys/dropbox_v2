<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getHome() {
        return view('home');
    }

    public function getFiles() {
        return view('myfiles');
    }

    public function getUpload() {
        return view('upload');
    }

    public function getLogin() {
        return view('login');
    }

    public function getLogout() {
        return view('logout');
    }

    public function getRegister() {
        return view('register');
    }
}
