<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

*/

Route::get('/', 'PagesController@getHome');
Route::get('/bestanden', 'FileController@getFiles');
Route::get('/upload', 'PagesController@getUpload');
Route::get('/inloggen', 'PagesController@getLogin');
Route::get('/uitloggen', 'PagesController@getLogout');
Route::get('/registreren', 'PagesController@getRegister');
Route::get('/download/{filename}', ['uses' => 'FileController@download']);
Route::get('/delete/{fileId}', ['uses' => 'FileController@delete']);

Route::post('/register/submit', 'RegisterController@register');
Route::post('/login/submit', 'LoginController@login');
Route::post('/logout/submit','LoginController@logout');
Route::post('/upload/store','FileController@storeFile');

