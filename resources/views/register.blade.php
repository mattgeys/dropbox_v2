@extends('layouts.app')

@section('content')
    <h1>Registreren</h1>
    {!! Form::open(['url' => 'register/submit']) !!}
    <div class="form-group col-sm-6">
        {{Form::label('firstname', 'Voornaam')}}
        {{Form::text('firstname', '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group col-sm-6">
        {{Form::label('lastname', 'Achternaam')}}
        {{Form::text('lastname', '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group col-sm-12">
        {{Form::label('email', 'E-Mail')}}
        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'email@example.com'])}}
    </div>
    <div class="form-group col-sm-12">
        {{Form::label('password', 'Wachtwoord')}}
        {{Form::password('password', ['class' => 'form-control'])}}
    </div>
    <div class="col-sm-12">
        {{Form::submit('Registreren', ['class' => 'btn btn-primary'])}}
    </div>
    {!! Form::close() !!}
@endsection