@extends('layouts.app')

@section('content')
        <h1>Mijn bestanden</h1>
        <a href='upload' class="btn btn-primary">Upload een bestand</a>
        @if(count($files) > 0)
            <table class="table">
                <thead>
                <tr>
                    <th>Naam</th>
                    <th>Uploaddatum</th>
                </tr>
                @foreach($files as $file)
                    <tr>
                        <td>{{$file->name}}</td>
                        <td>{{$file->created_at}}</td>
                        <td><a href="/download/{{$file->name}}" download="{{$file->name}}"><span
                                        class="glyphicon glyphicon-download"></span></a>&nbsp&nbsp<a
                                    href="/delete/{{$file->id}}"><span class="glyphicon glyphicon-remove"></span></a>
                        </td>
                    <tr>
                @endforeach
                </thead>
            </table>
    @endif
@endsection