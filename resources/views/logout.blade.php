@extends('layouts.app')

@section('content')
    <h1>Uitloggen</h1>
    <h3>Wilt u uitloggen?</h3>
    {!! Form::open(['url' => 'logout/submit']) !!}
    {{Form::submit('Uitloggen', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection