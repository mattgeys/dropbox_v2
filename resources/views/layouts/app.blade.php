<html>
<head>
    <meta charset="utf-8">
    <title>Acme</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<div class="container">
    <div class="jumobtron text-left">
        <div class="container">
            <h1>Dropbox v2</h1>
            <p class="lead">By Geysen Matthias</p>
        </div>
    </div>
</div>
@include('inc.navbar')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-8">
            @include('inc.errors')
            @yield('content')
        </div>
    </div>
</div>
<footer id="footer" class="text-center navbar-fixed-bottom">
    <p>Copyright 2017 &copy; Dropbox v2</p>
    <p>Matthias Geysen</p>
</footer>
</body>
</html>
