@extends('layouts.app')

@section('content')
    <h1>Upload een bestand</h1>
    {!! Form::open(['url' => 'upload/store', 'files' => true]) !!}
    {{csrf_field()}}
    <div class="form-group">
        {{Form::file('file')}}
    </div>
    <div class="form-group">
        {{Form::label('name', 'Naam')}}
        {{Form::text('name', '', ['class' => 'form-control'])}}
    </div>
    {{Form::submit('Uploaden', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection