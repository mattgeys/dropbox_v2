@extends('layouts.app')

@section('content')
    @if(session('fail'))
    <div class="alert alert-danger">
        {{session('fail')}}
    </div>
    @endif

    <h1>Inloggen</h1>
    {!! Form::open(['url' => 'login/submit']) !!}
    <div class="form-group">
        {{Form::label('email', 'E-Mail')}}
        {{Form::text('email', '', ['class' => 'form-control', 'placeholder' => 'email@example.com'])}}
    </div>
    <div class="form-group">
        {{Form::label('password', 'Wachtwoord')}}
        {{Form::password('password', ['class' => 'form-control'])}}
    </div>
        {{Form::submit('Inloggen', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection