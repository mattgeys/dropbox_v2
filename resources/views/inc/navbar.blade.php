<?php
session_start();
?>

<nav id='navbar' class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="{{Request::is('/') ? 'active' : ''}}"><a href="/">Home</a></li>
                @if(isset($_SESSION['email']))
                    <li class="{{Request::is('bestanden') ? 'active' : ''}}"><a href="/bestanden">Mijn bestanden</a></li>
                    @endif
            </ul>
            @if(isset($_SESSION['email']))
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/uitloggen">{{$_SESSION['email']}}</a></li>
                </ul>
            @else
                <ul class="nav navbar-nav navbar-right">
                    <li class="{{Request::is('inloggen') ? 'active' : ''}}"><a href="/inloggen">Inloggen</a></li>
                    <li class="{{Request::is('registreren') ? 'active' : ''}}"><a href="/registreren">Registeren</a>
                    </li>
                </ul>
            @endif;
        </div><!--/.nav-collapse -->
    </div>
</nav>